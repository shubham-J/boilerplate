# Boilerplate - A demo project for the demo 

This is boilerplate demo for the `README.md` file. Pick the suitable content for your project and fill it up with the necessary information, and provide information to your target audience.

## Description

This project is about Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt, enim sed pretium porttitor, justo risus laoreet elit, at tempus lorem nisl eget sem. Proin orci enim, condimentum non dapibus nec, luctus vel ex. In hac habitasse platea dictumst. Morbi viverra sem ligula, vehicula suscipit ante fermentum nec. Aliquam quis orci sit amet turpis laoreet tempor. Fusce dolor odio, venenatis ut mattis sed, consectetur vitae nisi. Etiam elementum commodo tortor id ultrices. Suspendisse potenti. Nulla urna ligula, tempor id blandit eget, egestas vel arcu. Pellentesque imperdiet egestas enim, eget fermentum ante. In ornare elit id libero dignissim eleifend. Nulla a quam ut lectus venenatis rhoncus at vitae quam. Morbi vitae velit nunc. In imperdiet dapibus arcu, quis laoreet velit tempus ac.

### Technology & Frameworks

[![bootstap](https://img.shields.io/badge/bootstrap-v5.1.2-blue)](https://getbootstrap.com/) [![jQuery](https://img.shields.io/badge/jQuery-v3.6.1-green)](https://jquery.com/download/)

[![Author](https://img.shields.io/badge/Author-Shubham%20Joshi-%23086AD8)]()
[![Author](https://img.shields.io/badge/Author-Shubham%20Joshi-%23C1549C)]() 
[![Author](https://img.shields.io/badge/Author-Shubham%20Joshi-%232CA18C)]()
[![Author](https://img.shields.io/badge/Author-Shubham%20Joshi-%23FF803B)]()
[![Author](https://img.shields.io/badge/Author-Shubham%20Joshi-%23C14364)]()
[![Author](https://img.shields.io/badge/Author-Shubham%20Joshi-%23EEE2D1)]()
[![Author](https://img.shields.io/badge/Author-Shubham%20Joshi-%23C2DD6A)]()
[![Author](https://img.shields.io/badge/Author-Shubham%20Joshi-%23805C4D)]()
[![Author](https://img.shields.io/badge/Maintainer-Shubham%20Joshi-%236F459E)]()

## Creator

[***White Orange Software***](https://whiteorangesoftware.com/)

## Owner

- **Dharmesh Mendapara**
- **Sanket Kyada**
- **Trupal Gadhiya**


## Authors

* UI/UX TEAM MEMBER
    * ***Devang Savaliya***
    * ***Divyesh Bhuva***

* FRONTEND TEAM MEMBER
    * ***Sampath Bingi***
    * ***Shubham Joshi***

* BACKEND TEAM MEMBER
    * ***Vivek Mangukiya***
    * ***Nikunj Ravaliya***


## Prerequisites

This project requires Java set up, node.js, react-bootstrap.

## Project Installation Notes

Write down the notes for project installation (Except simple HTML project)

* React \
*-* This is sample note for the installation notes \
*-* Make sure you have node version v.14 or above.

* Angular \
*-* This is sample note for the installation notes \
*-* Make sure you have node version v.16 or above.

* Tailwind \
*-* This is sample note for the installation notes \
*-* Make sure you have node version v.16 or above.


## Project Installation Steps

*-* Take a clone file of the git code. \
*-* Configure project details with your editor \
*-* Now follow given below steps:

These given steps are for only sample purpose
```
// For install necessary packages which is required for the project
npm i

// Run project on the browser - Start a package
npm start

// Run project on the browser - Stop a package
npm start

//Create a new branch
git -b [<branch-name>]

//Switch to a branch
git checkout [<branch-name>]

//Switch to new branch with creating a new branch
git checkout  -b [<branch-name>]

//Check current branch Status
git status
```

## Test & Deployment

*-* Take a clone file of the git code. \
*-* Configure project details with your editor \
*-* Now follow given below steps:

These given steps are for only sample purpose
```
// For run build - Build a package 
npm build
npm build [<package-folder>]

//Publish a package
npm publish [<folder>]
```

## Project Links

Home - [***https://whiteorangesoftware.com/***](https://whiteorangesoftware.com/) \
About - [***http://www.whiteorangesoftware.com/about***](http://www.whiteorangesoftware.com/about) \
Work - [***http://www.whiteorangesoftware.com/work***](http://www.whiteorangesoftware.com/work) \
Jobs - [***http://www.whiteorangesoftware.com/jobs***](http://www.whiteorangesoftware.com/jobs)\
Apply Online - [***http://www.whiteorangesoftware.com/apply-online***](http://www.whiteorangesoftware.com/apply-online)


## Support & References
Must use these links for making the README.md file.

[***https://github.com/tchapi/markdown-cheatsheet/blob/master/README.md***](https://github.com/tchapi/markdown-cheatsheet/blob/master/README.md)

[***Badge/Shield Generator***](https://shields.io/category/version)

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.


## Copyright
***2022 &COPY; whiteorangesoftware.com***

> Thank you for coming down here. However now you have come down, so now you can buy me(Shubham Joshi) a ***Lassi***. 🍶 Thanks!!